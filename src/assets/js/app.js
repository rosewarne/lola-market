import $ from 'jquery';
import whatInput from 'what-input';

window.$ = $;

// import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
import './lib/foundation-explicit-pieces';

  //Accordion Menu
    $('[data-accordion-menu] .categories-nav__item--top > span').click(function(e){
      var $this = $(this);
      var $itemParent = $('[data-accordion-menu] .categories-nav__item--top ');
      if ($this.parent().hasClass("is-active")) {
        $this.parent().removeClass("is-active");
      } else {
        $itemParent.removeClass("is-active");
        $this.parent().addClass("is-active");
      }
    });

// const axios = require("axios");

// const url =
//   "http://api.comprea.com/user/session";

// axios
//   .get(url)
//   .then(response => {
//     console.log(
//       `Status: ${response.status} -`,
//       `Token: ${response.token}`
//     );
//   })
//   .catch(error => {
//     console.log(error);
//   });

//   $.ajax({
//     url: "http://api.comprea.com/user/session",
//     dataType : 'json',
//     type: 'GET',
//     beforeSend : function(xhr) {
//         xhr.setRequestHeader("Accept", "application/json");
//         xhr.setRequestHeader("Content-Type", "application/json");
//         xhr.setRequestHeader("Authorization", "Bearer XXXX");
//     },
//     error : function() {
//         // error handler
//     },
//     success: function(data) {
//         console.log(data);
//         return data;
//     }
//   });